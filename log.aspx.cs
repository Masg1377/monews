﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.Configuration;

using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

using System.Globalization;
using System.IO;

public partial class log : System.Web.UI.Page
{
    string cs = WebConfigurationManager.ConnectionStrings["all"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["hacker"] != null )
        {
            error.InnerHtml = "هکر بی تربیت . . . ";
        }
    }
    protected void log_click(object sender, EventArgs e)
    {
        Account ac = new Account();
        ac.uname = log_uname.Value;
        ac.pass = log_pass.Value;

        int[] result = ac.Log_In();

        if (result[0] == 0)
        {
            error.InnerHtml = " رمز ورود اشتباه است . . .   ";
        }
        else if (result[0] == 1)
        {
            
            Session["log"] = result[1];

           Response.Redirect("~/admin/admin.aspx");
           error.InnerHtml = "لاگین شد . . .   ";

        }
    }

}