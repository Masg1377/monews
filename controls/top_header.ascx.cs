﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.Configuration;

using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

using System.Globalization;
using System.IO;

public partial class controls_top_header : System.Web.UI.UserControl
{
    string cs = WebConfigurationManager.ConnectionStrings["all"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["log"] != null)
        {
            welcome();
        }
        else
        {
            Response.Redirect("~/log.aspx?hacker=yes");
        }
    }

    void welcome()
    {
        int uid = (int)Session["log"];
        SqlConnection con = new SqlConnection(cs);
        SqlCommand cmd = new SqlCommand("select fname,lname,uname,pic,sex from account where id=@id", con);

        cmd.Parameters.AddWithValue("@id", uid);

        SqlDataReader reader;

        try
        {
            con.Open();
            reader = cmd.ExecuteReader();

            reader.Read();

            user_pic.Src = "~/images/uploads/users/full/" + reader["pic"];

            string fullName = reader["fname"] + " " + reader["lname"] + " (" + reader["uname"] + ")";
            string user_sex = (bool)reader["sex"] ? " آقای "  : " خانوم  ";

            wel.InnerHtml = user_sex + fullName;
            if(getUname() == "admin")
                 title.InnerHtml = "پنل ادمین";

        }
        catch { }
        finally
        {
            con.Close();
        }

    }
    protected string getUname()
    {
        int uid = (int)Session["log"];
        string uname = "";
        SqlConnection con = new SqlConnection(cs);
        SqlCommand cmd = new SqlCommand("select userType from account where id=@id", con);

        cmd.Parameters.AddWithValue("@id", uid);

        SqlDataReader reader;

        try
        {
            con.Open();
            reader = cmd.ExecuteReader();

            reader.Read();

            uname = reader["userType"] + "";

        }
        catch { }
        finally
        {
            con.Close();
        }
        return uname;
    }
}