﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="log.aspx.cs" Inherits="log" %>
<!DOCTYPE html>

<html>
<head>
    <link href="style/base.css" rel="stylesheet" />
    <link href="style/2 panel.css" rel="stylesheet" />
    <link href="style/form.css" rel="stylesheet" />
    <link href="style/sign.css" rel="stylesheet" />

</head>
<body>
    <form runat="server">
        <div id="content" style="background:#222; height:2000px;">
            <div class="holder" style="box-shadow:1px 1px 15px 1px #ccc;">


                <div class="center">
                    <div id="form">
                        <h2> ورود </h2>


                        <div id="error" runat="server">   </div>

                        <div class="items">
                            <span class="title">نام کاربری </span>
                            <input type="text" id="log_uname" runat="server" placeholder="فارسی وارد شود . . ." />
                            <span class="error"></span>
                        </div>

                        <div class="items">
                            <span class="title">رمز عبور</span>
                            <input type="password" id="log_pass" runat="server" placeholder="با طول حداقل 8 " />
                            <span class="error"></span>
                        </div>

                        <div class="items">
                            <input type="submit" value="ورود" runat="server" onserverclick="log_click"/>
                        </div>

                        <a href="sign.aspx" >ثبت نام  </a>

                    </div>
                </div>

            </div>
        </div>
    </form>
</body>
</html>

