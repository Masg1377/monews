﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="admin.aspx.cs" Inherits="admin_admin" %>

<%@ Register Src="~/controls/accardion.ascx" TagPrefix="uc1" TagName="accardion" %>
<%@ Register Src="~/controls/top_header.ascx" TagPrefix="uc1" TagName="top_header" %>



<!DOCTYPE html>

<html>
<head>
    <link href="../style/base2.css" rel="stylesheet" />
    <link href="../style/header2.css" rel="stylesheet" />
    <link href="../style/2 panel.css" rel="stylesheet" />
    <link href="../style/form.css" rel="stylesheet" />
    <link href="../style/accardion.css" rel="stylesheet" />
    <link href="../style/dash.css" rel="stylesheet" />
    <link href="../style/responsive.css" rel="stylesheet" />

</head>

<body>
    <form runat="server">

        <div id="header">
            <uc1:top_header runat="server" ID="top_header" />

        </div>
        <div id="content">
            <div class="holder" style="background:none;">

                <div class="right">
                    <!-- accardion  -->

                    <uc1:accardion runat="server" ID="accardion" />



                </div>

                <div class="center">
                    <div class="dash" style="border:3px solid #888; border-radius:10px; height:500px;"> 

                        <div class="items">
                            <div class="header">
                                <span class="pic"></span>
                                <h2>اسلایدر</h2>
                                <a href="slider/add_slide.aspx">افزودن اسلاید</a>
                                <a href="slider/all_slide.aspx">مشاهده اسلاید ها</a>
                            </div>
                        </div>

                        <div class="items">
                            <div class="header">
                                <span class="pic"></span>
                                <h2>اخبار</h2>
                                <a href="news/add_news.aspx">افزودن خبر</a>
                                <a href="news/all_news.aspx">مشاهده اخبار</a>
                            </div>
                        </div>


                        <div class="items">
                            <div class="header">
                                <span class="pic"></span>
                                <h2>پروفایل</h2>
                                <a href="Account/alter_account.aspx?id=1000">ویرایش مشخصات</a>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <div id="footer"></div>


    </form>

</body>
</html>
