﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="add_slide.aspx.cs" Inherits="admin_slide_add_slide" %>

<%@ Register Src="~/controls/accardion.ascx" TagPrefix="uc1" TagName="accardion" %>
<%@ Register Src="~/controls/top_header.ascx" TagPrefix="uc1" TagName="top_header" %>



<!DOCTYPE html>

<html>
<head>
    <link href="../../style/base2.css" rel="stylesheet" />
    <link href="../../style/header2.css" rel="stylesheet" />
    <link href="../../style/2 panel.css" rel="stylesheet" />
    <link href="../../style/form.css" rel="stylesheet" />
    <link href="../../style/accardion.css" rel="stylesheet" />
    <link href="../../style/responsive.css" rel="stylesheet" />

    <script src="../../script/jquery.js"></script>
    <script src="../../script/myScript.js"></script>
    <script>

        $(document).ready(function () {

            accar_tune(0, 0)

        });

    </script>


</head>
<body>
    <form runat="server" onsubmit="return validate_form()">

        <div id="header">
            <uc1:top_header runat="server" ID="top_header" />
        </div>
        <div id="content">
            <div class="holder">

                <div class="right">
                    <!-- accardion  --> 
                    <uc1:accardion runat="server" ID="accardion" />

                </div>
                <div id="center" class="center">
                    <div id="form">
                        <h2>افزودن اسلاید </h2>

                        <div id="error" runat="server">      </div>

                        <div class="items">
                            <span class="title">عنوان اسلاید </span>
                            <input type="text" id="sli_title" runat="server" placeholder="فارسی وارد شود . . ." />
                            <span class="error"></span>
                        </div>

                        <div class="items s1">
                            <span class="title">شروع نمایش </span>
                            <input type="text" id="sli_start" runat="server" placeholder="به فرمت تاریخ : 1398/07/02 . . ." />
                            <span class="error"></span>
                        </div>

                        <div class="items s1">
                            <span class="title">پایان نمایش </span>
                            <input type="text" id="sli_end" runat="server" placeholder="به فرمت تاریخ : 1398/07/02 . . ." />
                            <span class="error"></span>
                        </div>


                        <div class="items s2">
                            <span class="title">اولویت نمایش </span>
                            <input type="text" id="sli_order" runat="server" placeholder="1-99" />
                            <span class="error"></span>
                        </div>


                        <div class="items">

                            <span class="title">محل نمایش </span>

                            <select id="sli_page" runat="server">
                                <option selected>صفحه اصلی </option>
                                <option>صفحه کالاها </option>
                                <option>صفحه اخبار </option>
                            </select>

                        </div>

                        <div class="items no_bg">
                            <span class="title">وضعیت نمایش </span>

                            <input type="radio" id="show" runat="server" name="r" checked />
                            <label for="show">نمایش</label>

                            <input type="radio" id="hide" runat="server" name="r" />
                            <label for="hide">عدم نمایش</label>
                        </div>

                        <div class="items">
                            <span class="title">عکس اسلاید  </span>

                            <input type="file" id="sli_pic" runat="server" />
                            <label for="sli_pic">آپلود </label>

                        </div>

                        <div class="items">
                            <span class="title">توضیحات </span>

                            <textarea id="sli_des" runat="server">    </textarea>
                        </div>

                        <div class="items">
                            <input type="submit" value="افزودن" runat="server" onserverclick="add_click" />
                            <input type="submit" value="پاک" runat="server" onserverclick="reset_click" />
                        </div>



                    </div>
                </div>

            </div>
        </div>
        <div id="footer"></div>


    </form>

</body>
</html>
