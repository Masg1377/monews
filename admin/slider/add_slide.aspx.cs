﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.Configuration;

using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

using System.Globalization;
using System.IO;

using System.Text.RegularExpressions;

public partial class admin_slide_add_slide : System.Web.UI.Page
{
    Slider slide = new Slider();

    string cs = WebConfigurationManager.ConnectionStrings["all"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void add_click(object sender, EventArgs e)
    {
        slide.title = sli_title.Value;
        slide.startDate = Int32.Parse(sli_start.Value.Replace("/", ""));
        slide.endDate = Int32.Parse(sli_end.Value.Replace("/", ""));
        slide.slideOrder = Int32.Parse(sli_order.Value);
        slide.targetPage = sli_page.Value;
        slide.show = show.Checked;
        slide.pic = myFile.uploadImage("sli_pic", "slides");
        slide.describe = sli_des.Value;

        slide.addSlide();
    }

    protected void reset_click(object sender, EventArgs e)
    {

    }


}