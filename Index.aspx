﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="style/base.css"/>
    <link rel="stylesheet" href="style/header.css"/>
    <link rel="stylesheet" href="style/slider.css"/>
    <link rel="stylesheet" href="style/imp-videos.css"/>
    <link rel="stylesheet" href="style/content.css"/>
    <link rel="stylesheet" href="style/footer.css"/>
</head>
<body>
    <form id="form1" runat="server">
        <div class="holder">
            <header class="header">
                <ul class="menu"> 
                    <li>صفحه اصلی</li>
                    <li>سیاسی</li>
                    <li>اجتماعی</li>
                    <li>اقتصادی</li>
                    <li>تماس با ما</li>
                </ul>
    
                <section class="search">
                    <input type="name" name="search" class="input" placeholder="جستجو..."/>
                    <span class="search-btn"></span>
                    <figure class="logo"></figure>
                </section>
            </header>

            <section id="slider">
	
                <article id="gal"> 
            
                    <figure class="items">  
                        <img src="image/slider/1.jpg" />
                        <figcaption>خبر مهم 1</figcaption>
                        <p>متن متن متن متن متن متن متن متن متن متن</p>
                    </figure>
            
                    <figure class="items">  
                        <img src="image/slider/2.jpg" />
                        <figcaption>خبر مهم 2</figcaption>
                        <p>متن متن متن متن متن متن متن متن متن متن</p>
                    </figure>
                    
                    <figure class="items">  
                        <img src="image/slider/3.jpg" />
                        <figcaption>خبر مهم 3</figcaption>
                        <p>متن متن متن متن متن متن متن متن متن متن</p>
                    </figure>
            
                    <figure class="items">  
                        <img src="image/slider/4.jpg" />
                        <figcaption>خبر مهم 4</figcaption>
                        <p>متن متن متن متن متن متن متن متن متن متن</p>
                    </figure>		
                    
                    <figure class="items">  
                        <img src="image/slider/5.jpg" />
                        <figcaption>خبر مهم 5</figcaption>
                        <p>متن متن متن متن متن متن متن متن متن متن</p>
                    </figure>	
                    
                </article>
            
                <div id="right" onclick="re()">  </div>	
                <div id="left" onclick="le()">   </div>	
            </section>

            <section class="imp-videos">
                <article class="title">
                    <p>ویدیوهای مهم روز</p>
                </article>

                <article class="mini-slider">
                    <input type="radio" id="it1" name="r" />
                    <input type="radio" id="it2" name="r"/>
                    <input type="radio" id="it3" name="r" />
                    <input type="radio" id="it4" name="r" />
                    <input type="radio" id="it5" name="r" />
                    <div class="gal">
                        <section class="items">
                            <img src="image/slider/1.jpg" />
                            <div class="footer">
                                <p>ویدیوی 1</p>
                            </div>
                            <figure class="play"></figure>
                        </section>
                        <section class="items">
                            <img src="image/slider/2.jpg" />
                            <div class="footer">
                                <p>ویدیوی 2</p>
                            </div>
                            <figure class="play"></figure>
                        </section>
                        <section class="items">
                            <img src="image/slider/3.jpg" />
                            <div class="footer">
                                <p>ویدیوی 3</p>
                            </div>
                            <figure class="play"></figure>
                        </section>
                        <section class="items">
                            <img src="image/slider/4.jpg" />
                            <div class="footer">
                                <p>ویدیوی 4</p>
                            </div>
                            <figure class="play"></figure>
                        </section>
                        <section class="items">
                            <img src="image/slider/5.jpg" />
                            <div class="footer">
                                <p>ویدیوی 5</p>
                            </div>
                            <figure class="play"></figure>
                        </section>
                    </div>

                    <div style="display: block; margin: 0 auto; width: 300px;">
                        <label class="lbl1" for="it1"><div></div></label>
                        <label class="lbl2" for="it2"><div></div></label>
                        <label class="lbl3" for="it3"><div></div></label>
                        <label class="lbl4" for="it4"><div></div></label>
                        <label class="lbl5" for="it5"><div></div></label>
                    </div>

                </article>
            </section>
            
            <section class="content">
                <article class="right">
                    <div class="title">
                        <p class="color">اخبار به روز</p>
                    </div>

                    <div class="news-holder">
                        <img src="image/content/news1.jpg" />
                        <div class="allnews">
                            <p>خبر 1</p>
                            <p>خبر 2</p>
                            <p>خبر 3</p>
                            <p>خبر 4</p>
                        </div>
                    </div>

                    <div class="title">
                        <p class="color2">سیاسی</p>
                    </div>

                    <div class="news-slide">
                        <input type="radio" name="l" id="l1"/>
                        <input type="radio" name="l" id="l2"/>
                        <input type="radio" name="l" id="l3"/>
                        <input type="radio" name="l" id="l4"/>

                        <div class="items" id="item1">
                            <img src="./image/content/news1.jpg"/>
                            <div class="news-slide-info">
                                <h2>تیتر خبر1</h2>
                                <p>متن متن متن متن متن متن متن متن متن متن متن متن</p>
                            </div>
                        </div>
                        <div class="items" id="item2">
                            <img src="./image/content/news1.jpg"/>
                            <div class="news-slide-info">
                                <h2>تیتر خبر2</h2>
                                <p>متن متن متن متن متن متن متن متن متن متن متن متن</p>
                            </div>
                        </div>
                        <div class="items" id="item3">
                            <img src="./image/content/news1.jpg"/>
                            <div class="news-slide-info">
                                <h2>تیتر خبر3</h2>
                                <p>متن متن متن متن متن متن متن متن متن متن متن متن</p>
                            </div>
                        </div>
                        <div class="items" id="item4">
                            <img src="./image/content/news1.jpg"/>
                            <div class="news-slide-info">
                                <h2>تیتر خبر4</h2>
                                <p>متن متن متن متن متن متن متن متن متن متن متن متن</p>
                            </div>
                        </div>

                        <label class="left" onclick="newsSlideLeft()"></label>
                        <label class="right" onclick="newsSlideRight()"></label>
                    </div>

                    <div class="title">
                        <p class="color2">اجتماعی</p>
                    </div>

                    <div class="news-slide">
                        <input type="radio" name="a" id="a1"/>
                        <input type="radio" name="a" id="a2"/>
                        <input type="radio" name="a" id="a3"/>
                        <input type="radio" name="a" id="a4"/>

                        <div class="items" id="items1">
                            <img src="./image/content/news1.jpg"/>
                            <div class="news-slide-info">
                                <h2>تیتر خبر1</h2>
                                <p>متن متن متن متن متن متن متن متن متن متن متن متن</p>
                            </div>
                        </div>
                        <div class="items" id="items2">
                            <img src="./image/content/news1.jpg"/>
                            <div class="news-slide-info">
                                <h2>تیتر خبر2</h2>
                                <p>متن متن متن متن متن متن متن متن متن متن متن متن</p>
                            </div>
                        </div>
                        <div class="items" id="items3">
                            <img src="./image/content/news1.jpg"/>
                            <div class="news-slide-info">
                                <h2>تیتر خبر3</h2>
                                <p>متن متن متن متن متن متن متن متن متن متن متن متن</p>
                            </div>
                        </div>
                        <div class="items" id="items4">
                            <img src="./image/content/news1.jpg"/>
                            <div class="news-slide-info">
                                <h2>تیتر خبر4</h2>
                                <p>متن متن متن متن متن متن متن متن متن متن متن متن</p>
                            </div>
                        </div>

                        <label class="left" onclick="newsSlideLeft2()"></label>
                        <label class="right" onclick="newsSlideRight2()"></label>
                    </div>

                    <div class="title">
                        <p class="color2">اقتصادی</p>
                    </div>

                    <div class="grid-news">
                        <div class="item">
                            <img src="./image/content/news1.jpg" />
                            <h2>تیتر 2</h2>
                            <p>متن متن متن متن متن متن متن متن متن متن متن...</p>
                        </div>
                        <div class="item">
                            <img src="./image/content/news1.jpg" />
                            <h2>تیتر 2</h2>
                            <p>متن متن متن متن متن متن متن متن متن متن متن...</p>
                        </div>
                        <div class="item">
                            <div class="news">
                                <header></header>
                                <h2>تیتر 2</h2>
                                <p>متن متن متن متن متن متن متن متن متن متن متن...</p>
                            </div>
                            <div class="news">
                                <header></header>
                                <h2>تیتر 2</h2>
                                <p>متن متن متن متن متن متن متن متن متن متن متن...</p>
                            </div>
                        </div>
                    </div>

                </article>

                <article class="left">
                    <div class="latest">
                        <header class="head"><p>آخرین اخبار</p></header>
                        <div class="news">
                            <p>متن متن متن متن</p>
                            <p>متن متن متن متن</p>
                            <p>متن متن متن متن</p>
                            <p>متن متن متن متن</p>
                            <p>متن متن متن متن</p>
                            <p>متن متن متن متن</p>
                            <p>متن متن متن متن</p>
                        </div>
                    </div>

                    <div class="latest">
                        <header class="head"><p>آخرین اخبار</p></header>
                        <div class="news">
                            <p>متن متن متن متن</p>
                            <p>متن متن متن متن</p>
                            <p>متن متن متن متن</p>
                            <p>متن متن متن متن</p>
                            <p>متن متن متن متن</p>
                            <p>متن متن متن متن</p>
                            <p>متن متن متن متن</p>
                        </div>
                    </div>
                </article>
            </section>

            <footer class="footer">
                <div class="top">
                    <div class="item">
                        <h2>پر بازدیدها</h2>
                        <div class="items">
                            <img src="./image/content/news1.jpg" />
                            <p>توضیحات توضیحات</p>
                        </div>
                        <div class="items">
                            <img src="./image/content/news1.jpg" />
                            <p>توضیحات توضیحات</p>
                        </div>
                    </div>
                    <div class="item">
                        <h2>برگزیده های هفته</h2>
                        <div class="items">
                            <img src="./image/content/news1.jpg" />
                            <p>توضیحات توضیحات</p>
                        </div>
                        <div class="items">
                            <img src="./image/content/news1.jpg" />
                            <p>توضیحات توضیحات</p>
                        </div>
                    </div>
                    <div class="item">
                        <h2>برگزیده های هفته</h2>
                        <div class="items">
                            <img src="./image/content/news1.jpg" />
                            <p>توضیحات توضیحات</p>
                        </div>
                        <div class="items">
                            <img src="./image/content/news1.jpg" />
                            <p>توضیحات توضیحات</p>
                        </div>
                    </div>

                </div>

                <div class="bottom">
                    <h3>درباره ما/تماس با ما/پیشنهادات و انتقادات</h3>
                    <div class="contact">
                        <div class="item"></div>
                        <div class="item"></div>
                        <div class="item"></div>
                        <div class="item"></div>
                    </div>
                </div>
            </footer>
        </div>
    </form>

<script>

pos = 0;
document.getElementById("l1").checked = true;
document.getElementById("a1").checked = true;
document.getElementById("it2").checked = true;
gal = document.getElementById('gal');
function re(){
    if(pos > 0){	
        pos = pos - 100;
        
        gal.style.left = pos + "%";
    }
    else if(pos == 0){	
        pos = 400;
        gal.style.left = pos + "%";
    }	
}
     
function le(){

    if(400 > pos){	
        pos = pos + 100;
        gal.style.left = pos + "%";
    }
    else if (pos == 400){
        pos =0;
        gal.style.left = pos + "%";
    }
}

function newsSlideLeft() {
   if(document.getElementById("l1").checked){
       document.getElementById("l2").checked = true;
   }
   else if(document.getElementById("l2").checked){
       document.getElementById("l3").checked = true;
   }
   else if(document.getElementById("l3").checked){
       document.getElementById("l4").checked = true;
   }
   else document.getElementById("l1").checked = true;
}
function newsSlideRight() {
   if(document.getElementById("l1").checked){
       document.getElementById("l4").checked = true;
   }
   else if(document.getElementById("l2").checked){
       document.getElementById("l1").checked = true;
   }
   else if(document.getElementById("l3").checked){
       document.getElementById("l2").checked = true;
   }
   else document.getElementById("l3").checked = true;
}

function newsSlideLeft2() {
   if(document.getElementById("a1").checked){
       document.getElementById("a2").checked = true;
   }
   else if(document.getElementById("a2").checked){
       document.getElementById("a3").checked = true;
   }
   else if(document.getElementById("a3").checked){
       document.getElementById("a4").checked = true;
   }
   else document.getElementById("a1").checked = true;
}
function newsSlideRight2() {
   if(document.getElementById("a1").checked){
       document.getElementById("a4").checked = true;
   }
   else if(document.getElementById("a2").checked){
       document.getElementById("a1").checked = true;
   }
   else if(document.getElementById("a3").checked){
       document.getElementById("a2").checked = true;
   }
   else document.getElementById("a3").checked = true;
}
</script>
</body>
</html>
