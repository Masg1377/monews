﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Account
/// </summary>
public class Account:DA
{
    public string fname;
    public string lname;
    public string uname;
    public string pass;
    public string userType;
    public bool sex;
    public string pic;
    public string describe;
    public int regDate;
    public int lastUpdate;
    public int [] Register()
    {
        base.connect();
        string sql = "account_insert";
        Dictionary<string, object> dic = new Dictionary<string, object>();

        dic.Add("@uname", uname);
        dic.Add("@pass", pass);
        dic.Add("@retvalue", true);

        int[] re = base.docommand(sql, "nottxt", dic);
        base.disconnect();
        return re;
    }

    public int [] Log_In()
    {
        base.connect();
        string sql = "account_verify";
        Dictionary<string, object> dic = new Dictionary<string, object>();
        dic.Add("@uname", uname);
        dic.Add("@pass", pass);
        dic.Add("@retvalue", "");
        dic.Add("@id", "");

        int[] re = base.docommand(sql, "nottxt", dic);
        base.disconnect();

        return re;
    }
}