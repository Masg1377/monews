﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Slider
/// </summary>
public class Slider:DA
{
    public int id;
    public string title;
    public int startDate;
    public int endDate;
    public int slideOrder;
    public string targetPage;
    public bool show;
    public string pic;
    public string describe;
    public int addDate;
    public int lastUpdate;

    public void addSlide()
    {
        base.connect();
        string ct = "insert into slide values(@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10)";
        Dictionary<string, object> dic = new Dictionary<string, object>();

        dic.Add("@p1", title);
        dic.Add("@p2", startDate);
        dic.Add("@p3", endDate);
        dic.Add("@p4", slideOrder);
        dic.Add("@p5", targetPage);
        dic.Add("@p6", show);
        dic.Add("@p7", pic);
        dic.Add("@p8", describe);
        dic.Add("@p9", myDate.getDate());
        dic.Add("@p10", myDate.getDate());

        base.docommand(ct, "txt", dic);
        base.disconnect();
    }
}