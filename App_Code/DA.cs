﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;

public class DA
{
    //string cs = WebConfigurationManager.ConnectionStrings["all"].ConnectionString;
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public DA()
    {
        con = new SqlConnection();
        cmd = new SqlCommand();
        da = new SqlDataAdapter();
        cmd.Connection = con;
        da.SelectCommand = cmd;
    }

    public void connect()
    {
        con.ConnectionString = WebConfigurationManager.ConnectionStrings["all"].ConnectionString;
        con.Open();
    }
    public void disconnect()
    {
        con.Close();
    }

    public DataSet Select(string sql)
    {
        cmd.CommandText = sql;
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }

    public int [] docommand(string sql, string cmdType, Dictionary<string, object> param)
    {
        bool retvalue = false;
        cmd.CommandText = sql;
        cmd.CommandType = cmdType == "txt" ? CommandType.Text : CommandType.StoredProcedure;

        foreach (var p in param)
        {
            if(!p.Key.Equals("@retvalue") && !p.Key.Equals("@id"))
            {
                cmd.Parameters.AddWithValue(p.Key, p.Value);
            }
            if(p.Key.Equals("@retvalue"))
            {
                cmd.Parameters.Add("@retvalue", SqlDbType.Int).Direction = ParameterDirection.Output;
                retvalue = true;
            }

            if (p.Key.Equals("@id"))
            {
                cmd.Parameters.Add("@id", SqlDbType.Int).Direction = ParameterDirection.Output;
            }
        }
        try
        {
            cmd.ExecuteNonQuery();
        }

        finally {}

        int result = 0;
        int uid = 0;
        if (retvalue)
        {
            result = (int)cmd.Parameters["@retvalue"].Value;
            if (result == 1)
            {
                uid = (int)cmd.Parameters["@id"].Value;
            }
        }
        int[] re = new int[2] { result, uid };
        return re;
    }
   
}