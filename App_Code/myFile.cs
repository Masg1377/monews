﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.IO;
using System.Drawing;

using System.Text;
using System.Security.Cryptography;


public class myFile
{
    public static string uploadImage(string inp, string folder)
    {
        // upload file

        string fileName = "223.png";
        string fileExt = "";

        HttpPostedFile file = HttpContext.Current.Request.Files[inp];

        if (file != null)
        {
            fileExt = Path.GetExtension(file.FileName);
            if (fileExt == ".jpg" || fileExt == ".jpeg" || fileExt == ".png")
            {
                
                fileName = Path.GetFileName(file.FileName);

                Random r = new Random();

                fileName = r.Next(1000, 1000000) + "_" + fileName;

                file.SaveAs(HttpContext.Current.Server.MapPath("~/image/uploads/" + folder + "/full/") + fileName);

                // resize


                Image im = Image.FromStream(file.InputStream);

                int newWidth = 200;

                float asp = (float)im.Size.Width / im.Size.Height;

                int newHeight = Convert.ToInt32(newWidth / asp);

                Bitmap bm = myResizeImage(im,newWidth,newHeight );

                bm.Save(HttpContext.Current.Server.MapPath("~/image/uploads/" + folder + "/thumbs/") + fileName);

            }
        }
        return fileName;
    }

    static Bitmap myResizeImage(Image originalImage,int nWidth,int nHeight)
    {
        Bitmap nb = new Bitmap(nWidth, nHeight);

        Graphics gr = Graphics.FromImage(nb);

        gr.DrawImage(originalImage, 0, 0, nWidth, nHeight);

        return nb;
    }

    public static string myHash(string pass)
    { 
        byte[] ar = Encoding.ASCII.GetBytes(pass);


        MD5CryptoServiceProvider x = new MD5CryptoServiceProvider();

        
        ar =  x.ComputeHash(ar);

        string np = Encoding.ASCII.GetString(ar);

        return np;
    }


}